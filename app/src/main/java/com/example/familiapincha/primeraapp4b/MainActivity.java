package com.example.familiapincha.primeraapp4b;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button botonLogin, botonRegistar, botonBuscar, botonParametro, botonfragmento;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botonParametro = (Button) findViewById(R.id.btnapasarparametro);
        botonLogin = (Button) findViewById(R.id.btnlogin);
        botonRegistar = (Button) findViewById(R.id.btnguardar);
        botonBuscar = (Button) findViewById(R.id.btnbuscar);
        botonfragmento = (Button) findViewById(R.id.btnFragmento);
//
        botonParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PasarParametro.class);
                startActivity(intent);
            }
        });

        botonfragmento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Fragmentos.class);
                startActivity(intent);
            }
        });
//
        botonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(MainActivity.this, ActividadLogin.class);
                startActivity(intent);

            }
        });

        botonRegistar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(MainActivity.this, ActividadRegistrar.class);
                startActivity(intent);

            }


        });

        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(MainActivity.this, ActividadBuscar.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.opcionLogin:
                Dialog dialogoLogin = new Dialog(MainActivity.this);
                dialogoLogin.setContentView(R.layout.dlg_login);

                Button btnAutenticar = (Button) dialogoLogin.findViewById(R.id.btnAutenticar);
                final EditText cajaUsuario =(EditText) dialogoLogin.findViewById(R.id.txtUser);
                final EditText cajaClave =(EditText) dialogoLogin.findViewById(R.id.txtPassword);

                btnAutenticar.setOnClickListener(new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {

                        Toast.makeText(MainActivity.this,cajaUsuario.getText().toString()
                                + " " + cajaClave.getText().toString(), Toast.LENGTH_LONG).show();

                    }

                });





                dialogoLogin.show();
                break;
        }
        switch (item.getItemId()){
            case R.id.opcionRegistrar:
                intent = new Intent(MainActivity.this, ActividadRegistrar.class);
                startActivity(intent);
                break;
        }

        return true;
    }
}